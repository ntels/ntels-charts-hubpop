# Hup-Pop ntels 서비스 배포를 위한 랜처 차트

Hub-Pop 서비스(API 게이트웨이, 실시간 데이터 연계, DBaaS, 개발 파이프라인) 배포를 위한 랜처 차트입니다.
 

## 구성

통합 차트: 하나의 서비스를 위한 모든 애플리케이션을 배포합니다.

```
common-server						# 공용: 아래의 애플리케이션들이 배포됩니다.
  - config-server					# 컨피그 서버
  - eureka-server					# 유레카 서버

api-gw							# API 게이트웨이: 아래의 애플리케이션들이 배포됩니다.
  - api-portal						# 사용자 포탈
  - api-mng-portal					# 관리자 포탈
  - api-gateway						# 게이트웨이 서버
  - hystrix-dashboard					# Hystrix 대시보드
  - oauth						# Oauth 서버
  - turbine-server					# Turbine 서버

rt-data							# 실시간 데이터 연계: 아래의 애플리케이션들이 배포됩니다.
  - data-portal						# 포탈
  - context-broker					# 컨텍스트 데이터, 메시지 데이터 서버
  - data-lake						# 테이블 데이터 서버

dbaas							# DBaaS: 아래의 애플리케이션들이 배포됩니다.
  - shared-portal					# 포탈
  - cassandra-hub					# 카산드라 허브
  - mongo-hub						# 몽고 허브
  - mssql-hub						# MS-SQL 허브
  - oracle-hub						# 오라클 허브

devops							# 개발 파이프라인: 아래의 애플리케이션들이 배포됩니다.
  - pipeline-portal					# 포탈
```

단일 차트: 하나의 애플리케이션을 배포합니다.

```
config-server						# 컨피그 서버
eureka-server						# 유레카 서버

api-portal						# API 게이트웨이 사용자 포탈
api-mng-portal						# API 게이트웨이 관리자 포탈
api-gateway						# 게이트웨이 서버
hystrix-dashboard					# Hystrix 대시보드
oauth							# Oauth 서버
turbine-server						# Turbine 서버

data-portal						# 실시간 데이터 포탈
context-broker						# 컨텍스트 데이터, 메시지 데이터 서버
data-lake						# 테이블 데이터 서버

shared-portal						# DBaaS 포탈
cassandra-hub						# 카산드라 허브
mongo-hub						# 몽고 허브
mssql-hub						# MS-SQL 허브
oracle-hub						# 오라클 허브

pipeline-portal						# 개발 파이프라인 포탈
```